<?php

namespace gdb;

require_once 'configuration.interface.php';



/**
 * @brief Interface for connect to database.
 * 
 */
interface i_database {
  /**
   * @brief Connect this object to the database with the parmetre in i_configuration object. 
   * 
   * @retval i_database the object connected to database
   * @param[in] i_configuration $configuration  Object with the configuration of project.
   */
  public function __construct(i_configuration $configuration);
  /**
   * @brief Return the list of table in database.
   * 
   * @retval array(2)  with column : Name, Engine, Version, Row_format, Rows, Avg_row_length, Data_length, Max_data_length, Index_length, Data_free, Auto_increment, Create_time, Update_time, Check_time, Collation, Checksum, Create_options and Comment
   */
  public function get_tables();
  public function get_database_name();
  public function get_columns($tablename);
  public function get_columns_view($tablename, $view);
  public function get_columns_insert($tablename, $insert);
  public function get_column_id($tablename);
  public function get_data($tablename, $columns);
  /**
   * return liste of column with flag insert
   * @warning $tablename isn't controlled
   * @todo document this function
   * @retval bool Indicator if insertion is ok.
   */
  public function insert_record($tablename, $data);
  /**
   * @warning $tablename isn't controlled
   * @todo document this function
   */
  public function delete_record($tablename, $id);
}
