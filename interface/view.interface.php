<?php

namespace gdb;

require_once 'interface/configuration.interface.php';
require_once 'interface/database.interface.php';



/**
 * @brief Interface for print a view.
 * 
 */
interface i_view {
  public function __construct(i_database $database, i_configuration $configuration);
  /**
   * @todo documenter fonction
   * 
   * @param array $request The table of parameter with syntaxe of $_REQUEST, $_GET or $_POST.
   * @retval bool Indication if you need call print_view or not.
   */
  public function exec_parameter($request);
  /**
   * @todo documenter fonction
   * 
   * @retval bool(false) If don't redirect require.
   * @retval string The url for redirect.
   */
  public function url_redirect();
  public function print_view();
}
