<?php

namespace gdb;

/**
 * @brief This interface is the stadard interface for configuration.
 * @details This interface is the stadard interface for configuration.
 * All configuration are implements this interface.
 * 
 */
interface i_configuration {
  /**
   * @brief Return the username for database connexion.
   * 
   * @retval string The username.
   */
  public function get_database_username();
  /**
   * @brief Return the password for connexion at the database.
   * 
   * @retval string The password for connexion.
   */
  public function get_database_password();
  /**
   * @brief Return address of database server.
   * 
   * @retval string The address or IP.
   */
  public function get_database_server();
  /**
   * @brief Return the port for connexion to database.
   * 
   * @retval int The port of server.
   */
  public function get_database_port();
  /**
   * @brief Return the charset for the communication whith the server.
   * 
   * @retval string Charset for server.
   */
  public function get_database_charset();
  /**
   * @brief Return the name of the database.
   * 
   * @retval string The name of database.
   */
  public function get_database_name();
  public function get_url_base();
  public function get_url_suffixe_tables();
  public function get_url_suffixe_table();
  public function get_url_suffixe_insert();
  public function get_url_suffixe_delete();
  public function get_lang();
  /**
   * @brief Indicator for database cache in object.
   * 
   * @retval bool If true the cache is possible actived.
   */
  public function get_database_cache();
}
