<?php
/**
 * Documentation, License etc.
 *
 * @package gdb
 */

namespace gdb_example;

require_once 'inc/initgdb.inc.php';

require_once 'interface/configuration.interface.php';
require_once 'template/database.template.php';



class example_configuration implements \gdb\i_configuration {
  public function get_database_username()   {return 'gbd';}
  public function get_database_password()   {return 'R2waDsznShRGLuZ7';}
  public function get_database_server()     {return 'localhost';}
  public function get_database_port()       {return 3306;}
  public function get_database_charset()    {return 'utf8';}
  public function get_database_name()       {return 'gbd';}
  public function get_url_base()            {return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/';}
  public function get_url_suffixe_tables()  {return 'tables';}
  public function get_url_suffixe_table()   {return 'table';}
  public function get_url_suffixe_insert()  {return 'insert';}
  public function get_url_suffixe_delete()  {return 'delete';}
  public function get_lang()                {return 'fr';}
  public function get_database_cache()      {return true;}
}




$config = new example_configuration;

$database = new \gdb\t_database($config);

// if user don't specified PATH_INFO, rediret it in correct page.
if (! isset($_SERVER['PATH_INFO'])) {
  header('Location: '.$config->get_url_base().'tables');
  die();
}

$view = null;
$matches = [];

if (preg_match('~^/tables/?$~', $_SERVER['PATH_INFO'])) {
  require_once 'template/view_tables.template.php';
  $view = new \gdb\t_view_tables($database, $config);
} elseif (preg_match('~^/table/([^/]+)$~', $_SERVER['PATH_INFO'], $matches)) {
  require_once 'template/view_table.template.php';
  $view = new \gdb\t_view_table($database, $config, $matches[1]);
} elseif (preg_match('~^/insert/([^/]+)$~', $_SERVER['PATH_INFO'], $matches)) {
  require_once 'template/view_insert.template.php';
  $view = new \gdb\t_view_insert($database, $config, $matches[1]);
} elseif (preg_match('~^/delete/([^/]+)$~', $_SERVER['PATH_INFO'], $matches)) {
  require_once 'template/view_delete.template.php';
  $view = new \gdb\t_view_delete($database, $config, $matches[1]);
} else {
  var_dump($_SERVER['PATH_INFO']);
  die();
}



if (!empty($_REQUEST)) {
  if ($view->exec_parameter($_REQUEST)) {
    $view->print_view();
  }
}
if (empty($_REQUEST)) {
  $view->print_view();
}

if(($url = $view->url_redirect()) !== false) {
  header('Location: '.$url);
  die();
}


// var_dump($view);
