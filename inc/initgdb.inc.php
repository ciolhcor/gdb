<?php

// set_include_path(get_include_path() . PATH_SEPARATOR . 'interface');
// set_include_path(get_include_path() . PATH_SEPARATOR . 'template');

namespace gdb;

require_once 'interface/configuration.interface.php';


function get_traduction_json($JSON, $word, i_configuration $configuration) {
  if(isset($JSON['JSON']['trad'][$configuration->get_lang()][$word])) return $JSON['JSON']['trad'][$configuration->get_lang()][$word];
  if (isset($JSON['JSON']['trad'])) {
    foreach($JSON['JSON']['trad'] as $lang => $content) if (isset($content[$word])) return $content[$word];
  }
  if (isset($JSON[$word])) return $JSON[$word];
  return false;
}
