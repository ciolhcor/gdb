<?php

namespace gdb;

require_once 'interface/view.interface.php';
require_once 'interface/database.interface.php';


abstract class t_view implements i_view {
  
  protected $database;
  protected $configuration;
  
  public function __construct(i_database $database, i_configuration $configuration) {
    $this->database = $database;
    $this->configuration = $configuration;
  }
  abstract public function print_view();
  abstract public function exec_parameter($request);
 
}
