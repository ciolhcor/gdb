<?php

namespace gdb;

require_once 'view.template.php';


class t_view_delete extends t_view {
  
  protected $lst_tables = null;
  protected $tablename = null;
  
  public function __construct(i_database $database, i_configuration $configuration, $tablename='') {
    parent::__construct($database, $configuration);
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    foreach($this->lst_tables as $table) if ($table['Name'] === $tablename) $this->tablename = $tablename;
    if ($this->tablename === null) throw new \InvalidArgumentException('This tablename isn\'t in database.');
  }
  
  public function print_view() {
  }
  public function exec_parameter($request) {
    if (isset($request['id'])) {
      $this->database->delete_record($this->tablename, $request['id']);
    }
    return false;
  }
  public function url_redirect() {
    return $this->configuration->get_url_base().$this->configuration->get_url_suffixe_table().'/'.$this->tablename;
  }
}
