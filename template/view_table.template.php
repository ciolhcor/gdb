<?php

namespace gdb;

require_once 'view.template.php';


class t_view_table extends t_view {
  
  protected $lst_tables = null;
  protected $tablename = null;
  
  public function __construct(i_database $database, i_configuration $configuration, $tablename='') {
    parent::__construct($database, $configuration);
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    foreach($this->lst_tables as $table) if ($table['Name'] === $tablename) $this->tablename = $tablename;
    if ($this->tablename === null) throw new \InvalidArgumentException('This tablename isn\'t in database.');
  }
  
  public function print_view() {
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    $column_id = $this->database->get_column_id($this->tablename);
    $columns = $this->database->get_columns_view($this->tablename, 'yes');
    $lst_columns = [];
    $column_id_is_inside = false;
    foreach($columns as $column) {
      $lst_columns[] = $column['Field'];
      if ($column == $column_id) $column_id_is_inside = true;
    }
    if (!$column_id_is_inside) $lst_columns[] = $column_id['Field'];
    
    $datas = $this->database->get_data($this->tablename, $lst_columns);
    ?>
    <table border="1">
      <thead>
        <tr>
          <?php 
          foreach($columns as $column) echo '<th>'.get_traduction_json($column, 'Field', $this->configuration).'</th>';
          $url_insert_row = $this->configuration->get_url_base().$this->configuration->get_url_suffixe_insert().'/'.$this->tablename;
          echo '<td colspan=3><a href="'.$url_insert_row.'">+</a></td>';
          ?>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <?php 
          foreach($columns as $column) echo '<th>'.get_traduction_json($column, 'Field', $this->configuration).'</th>';
          echo '<td colspan=3><a href="'.$url_insert_row.'">+</a></td>';
          ?>
        </tr>
      </tfoot>
      <tbody>
        <?php foreach($datas as $data) { ?>
          <tr>
            <?php 
              foreach($data as $key => $value) {
                if ($column_id_is_inside || $key != $column_id['Field']) {
                  echo "<td>$value</td>";
                }
              }
              /// @todo lien vers View Row
              echo '<td><a href="#lienversViewRow">V</a></td>';
              /// @todo lien vers Edit Row
              echo '<td><a href="#lienversEditRow">E</a></td>';
              /// @todo lien vers Delete Row
              $url_delete_row = $this->configuration->get_url_base().$this->configuration->get_url_suffixe_delete().'/'.$this->tablename.'?id='.$data[$column_id['Field']];
              echo '<td><a href="'.$url_delete_row.'">X</a></td>';
            ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php
  }
  public function exec_parameter($request) {return true;}
  public function url_redirect() {return false;}
}
