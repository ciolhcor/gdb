<?php

namespace gdb;

require_once 'view.template.php';


class t_view_insert extends t_view {
  
  protected $lst_tables = null;
  protected $tablename = null;
  protected $columns_insert = null;
  
  public function __construct(i_database $database, i_configuration $configuration, $tablename='') {
    parent::__construct($database, $configuration);
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    foreach($this->lst_tables as $table) if ($table['Name'] === $tablename) $this->tablename = $tablename;
    if ($this->tablename === null) throw new \InvalidArgumentException('This tablename isn\'t in database.');
  }
  
  public function print_view() {
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    if ($this->columns_insert === null) $this->columns_insert = $this->database->get_columns_insert($this->tablename, 'yes');
    echo '<form>';
    foreach($this->columns_insert as $column) {
      $id = $this->database->get_database_name().'$'.$this->tablename.'$'.$column['Field'];
      echo '<label for="id_form.'.$id.'">'.get_traduction_json($column, 'Field', $this->configuration).'</label> ';
      echo '<input type="text" name="'.$id.'" id="id_form'.$id.'" /><br />';
    }
    echo '<input type="submit" />';
    echo '</form>';
  }
  public function exec_parameter($request) {
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    if ($this->columns_insert === null) $this->columns_insert = $this->database->get_columns_insert($this->tablename, 'yes');
    if (empty($request)) return true;
    
    $data = [];
    foreach($this->columns_insert as $column) {
      $id = $this->database->get_database_name().'$'.$this->tablename.'$'.$column['Field'];
      if (!isset($request[$id])) return false;
      else $data[$column['Field']] = $request[$id];
    }
    if ($this->database->insert_record($this->tablename, $data)) {
      return false;
    } else {
      return false;
    }
  }
  public function url_redirect() {
    return $this->configuration->get_url_base().$this->configuration->get_url_suffixe_table().'/'.$this->tablename;
  }
}
