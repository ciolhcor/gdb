<?php

namespace gdb;

require_once 'interface/database.interface.php';
require_once 'interface/configuration.interface.php';



class t_database implements i_database {
  
  private $database_name = null;
  private $pdo = null;
  
  private $cache = false;
  
  private $lst_tables = null;
  private $array_lst_columns = [];
  
  public function __construct(i_configuration $configuration) {
    $this->database_name = $configuration->get_database_name();
    $this->cache = $configuration->get_database_cache();
    $this->pdo = new \PDO('mysql:host='.$configuration->get_database_server().';port='.$configuration->get_database_port().';dbname='.$this->database_name.';charset='.$configuration->get_database_charset(), 
      $configuration->get_database_username(), 
      $configuration->get_database_password(), 
      [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING, \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC]);
  }
  
  
  public function get_tables(){
    if ($this->cache && $this->lst_tables !== null) return $this->lst_tables;
    else {
      $datas_return = [];
      $datas = $this->pdo->query("SHOW TABLE STATUS")->fetchall();
      foreach($datas as $data) {
        $datas_return[$data['Name']] = $data;
        $datas_return[$data['Name']]['JSON'] = json_decode($data['Comment'], true);
      }
      return $this->lst_tables = $datas_return;
    }
  }
  public function get_database_name() {
    return $this->database_name;
  }
  public function get_columns($tablename) {
    if ($this->cache && isset($this->array_lst_columns[$tablename])) return $this->array_lst_columns[$tablename];
    else {
      $lst_tables = $this->get_tables();
      if (isset($lst_tables[$tablename])) {
        $datas_return = [];
        $datas = $this->pdo->query("SHOW FULL COLUMNS FROM `$tablename`")->fetchall();
        foreach($datas as $data) {
          $datas_return[$data['Field']] = $data;
          $datas_return[$data['Field']]['JSON'] = json_decode($data['Comment'], true);
        }
        return $this->array_lst_columns[$tablename] = $datas_return;
      } else {
        throw new \InvalidArgumentException("This tablename isn't correct : $tablename");
      }
    }
  }
  public function get_columns_view($tablename, $view) {
    $datas = [];
    foreach($this->get_columns($tablename) as $data)
      if (isset($data['JSON']['view']) && $data['JSON']['view'] === $view) $datas[] = $data;
      elseif (!isset($data['JSON']['view'])) $datas[] = $data;
    return $datas;
  }
  public function get_columns_insert($tablename, $insert) {
    $datas = [];
    foreach($this->get_columns($tablename) as $data)
      if (isset($data['JSON']['insert']) && $data['JSON']['insert'] === $insert) $datas[] = $data;
      elseif (!isset($data['JSON']['insert'])) $datas[] = $data;
    return $datas;
  }
  public function get_column_id($tablename) {
    $datas = [];
    foreach($this->get_columns($tablename) as $data)
      if ($data['Key'] === 'PRI') return $data;
    return false;
  }
  public function get_data($tablename, $columns) {
    $lst_columns = $this->get_columns($tablename);
    $sql = "";
    if (!empty($lst_columns)) {
      if (empty($columns)) {
        $sql = "SELECT * FROM `$tablename`";
      }
      else {
        foreach($columns as $column) if (!isset($lst_columns[$column])) throw new \InvalidArgumentException("This column name isn't correct : $column");
        $sql = "SELECT `".implode('`, `', $columns)."` FROM `$tablename`";
      }
    }
    $datas = $this->pdo->query($sql)->fetchall();
    return $datas;
  }
  public function insert_record($tablename, $data) {
    $sql = 'INSERT INTO `'.$tablename.'` SET ';
    $sql_array = [];
    foreach($data as $key => $value) {
      $sql_array[] = "$key = :$key";
    }
    $sql .= implode(', ', $sql_array);
    $request = $this->pdo->prepare($sql);
    return $request->execute($data);
  }
  public function delete_record($tablename, $id) {
    $column_id = $this->get_column_id($tablename);
    $sql = 'DELETE FROM `'.$tablename.'` WHERE `'.$column_id['Field'].'` = :id';
    $request = $this->pdo->prepare($sql);
    return $request->execute(['id' => $id]);
  }
}