<?php

namespace gdb;

require_once 'view.template.php';


class t_view_tables extends t_view {
  
  protected $lst_tables = null;
  
  public function print_view() {
    if ($this->lst_tables === null) $this->lst_tables = $this->database->get_tables();
    echo '<ul>';
    foreach($this->lst_tables as $table) {
      $view = true;
      if (isset($table['JSON']['view']) && $table['JSON']['view'] != 'yes') $view = false;
      if ($view) {
        $url = $this->configuration->get_url_base();
        $url .= $this->configuration->get_url_suffixe_table();
        $url .= '/'.$table['Name'];
        $text = get_traduction_json($table, 'Name', $this->configuration);
        echo '<li><a href="'.$url.'">'.$text.'</a></li>';
      }
    }
    echo '</ul>';
  }
  public function exec_parameter($request) {return true;}
  public function url_redirect() {return false;}
}
